package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
    
    ArrayList<FridgeItem> fridgelist = new ArrayList<FridgeItem>();
    int max_size = 20; 


    @Override
    public int totalSize(){
        return max_size;
    }
    

    @Override
    public int nItemsInFridge() {
        return this.fridgelist.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (fridgelist.size() <20){
            fridgelist.add(item);
            return true;
        }
        else{
            return false;  
        }
        
    }

    @Override
    public void takeOut(FridgeItem item){
        if (!fridgelist.isEmpty()){
            for (FridgeItem i : this.fridgelist){
                if (i.equals(item)){
                    fridgelist.remove(i);
                    break;
                   }       }}
        else {
            throw new NoSuchElementException();
        }    
        
    }

    @Override
    public void emptyFridge() {
        fridgelist.clear();
        
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> goodFood = new ArrayList<>();
        for(FridgeItem item: fridgelist) {
            if (item.hasExpired()== false){
                goodFood.add(item);
                String itemNameitem =item.getName();
                System.out.println(itemNameitem);
            }
        }
        fridgelist.removeAll(goodFood);
        return fridgelist;
    }
}
